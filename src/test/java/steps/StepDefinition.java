package steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import animal.Cat;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefinition {
	Cat cat;
	
	@Given("^I have an empty cat$")
	public void i_have_an_empty_cat() throws Exception {
		assertNull(cat);
	}

	@When("^I create with the name \"([^\"]*)\"$")
	public void i_create_with_the_name(String name) throws Exception {
		cat = new Cat(name);
	}

	@Then("^The name returned should be \"([^\"]*)\"$")
	public void the_name_returned_should_be(String name) throws Exception {
		assertEquals(name, cat.getName());
	}
	
	/*
	
	@Given("^I have an empty cat$")
	public void I_have_an_empty_cat() {
		assertNull(cat);
	}
	
	@When("^I create with the name \"([^\"]*)\"$")
	public void I_create_with_the_name(String name) {
		cat = new Cat(name);
	}
	
	@When("^The name returned should be \"([^\"]*)\"$")
	public void The_name_returned_should_be(String name) {
		assertEquals(name, cat.getName());
	}*/
	
	
}
