package steps;

import static org.junit.Assert.assertEquals;

import calculator_kata.StringCalculator;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StringCalculatorSteps {
	StringCalculator sc = new StringCalculator();
	String input;
	
	@Given("^An empty string$")
	public void an_empty_string() throws Exception {
		input = "";
	}

	@When("^I pass the empty string \"([^\"]*)\"$")
	public void i_pass_the_empty_string(String input) throws Exception {
		assertEquals("", input );
		this.input = input;
	}

	@Then("^The result should be (\\d+)$")
	public void the_result_should_be(int result) throws Exception {
		assertEquals(result, sc.add(input));
	}
}
