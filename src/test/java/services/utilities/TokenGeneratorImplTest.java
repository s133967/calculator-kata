package services.utilities;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import services.user.TokenGenerator;
import services.user.TokenGenerator.InvalidNumberOfTokensException;

public class TokenGeneratorImplTest {
	TokenGenerator tokenGenerator;
	
	@Before
	public void beforeEach() {
		tokenGenerator = new TokenGeneratorImpl();
	}
	
	@Test(expected = InvalidNumberOfTokensException.class)
	public void givenLessThanZeroNumberOfTokensRequested_returnInvalidNumberOfTokensException() {
		tokenGenerator.generate("basic", -1);
	}
	
	@Test(expected = InvalidNumberOfTokensException.class)
	public void givenZeroNumberOfTokensRequested_returnInvalidNumberOfTokensException() {
		tokenGenerator.generate("basic", 0);
	}
	
	@Test(expected = InvalidNumberOfTokensException.class)
	public void givenMoreThanFiveNumberOfTokensRequested_returnInvalidNumberOfTokensException() {
		tokenGenerator.generate("basic", 6);
	}
	
	@Test
	public void givenCorrectNumberOfTokensRequested_returnNumberOfTokensRequested() {
		List<String> tokens = tokenGenerator.generate("basic", 3);
		assertEquals(3, tokens.size());
	}
}
