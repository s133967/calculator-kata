package services.entities;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TokenTest {
	Token token;
	
	@Before
	public void before() {
		token = new Token();
	}
	
	
	@Test
	public void givenNewToken_returnTokenStatusUnused() {
		assertEquals("unused", token.getStatus());
	}
	
	@Test
	public void givenUsedToken_returnTokenStatusUsed() {
		//arrange
		token.setStatus("used");
		
		//act 
		//assert
		assertEquals("used", token.getStatus());
	}
	
	@Test
	public void givenNewToken_returnTokenIdIsEmptyString() {
		assertEquals("", token.getId());
	}

	@Test
	public void givenSetTokenId_returnTokenId() {
		token.setId("123");
		assertEquals("123", token.getId());
	}
	
	
}
