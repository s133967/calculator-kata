package services.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import services.entities.Token;
import services.entities.User;
import services.user.UserService.InvalidUserEmailException;
import services.user.UserService.MoreThanOneTokenLeftException;
import services.user.UserService.TokenUsedException;
import services.utilities.TokenGeneratorImpl;

public class UserServiceImplTest {
	UserServiceImpl userService;
	
	UserGateway userGateway = new UserGatewayImpl();
	TokenGenerator tokenGenerator = new TokenGeneratorImpl();

	@Before
	public void runsBeforeEachTest() {
		userService = new UserServiceImpl();
		userService.setUserGateway(userGateway);
		userService.setTokenGenerator(tokenGenerator);
	}

	private void addAUserWithTokenToUserGateway() {
		UserGateway userGateway = new UserGatewayImpl();
		User user = new User();
		user.addTokens(new ArrayList<Token>( Arrays.asList(new Token("Token")) ));
		userGateway.addUser(user);
		userService.setUserGateway(userGateway);
	}
	
	@Test(expected = InvalidUserEmailException.class)
	public void givenEmptyEmail_returnInvalidUserEmailException() {
		// act
		userService.createUser("");
	}

	@Test
	public void givenCreatingAnUserByEmail_returnUserEmailAsId() {
		// act
		assertEquals("kaloyan@penov.com", userService.createUser("kaloyan@penov.com"));
	}

	@Test
	public void givenGettingUserByEmailAsId_returnUserWithTheSameEmailAsId() {
		// arrange
		// act
		userService.createUser("kaloyan@penov.com");
		User user = userService.getUser("kaloyan@penov.com");
		// assert
		assertEquals("kaloyan@penov.com", user.getEmail());
	}

	@Test
	public void givenNewUserCreated_returnTheSameUserWith5TokensByDefault() {
		// arrange
		// act
		// new user
		userService.setTokenGenerator(new TokenGenerator() {
			@Override
			public List<String> generate(String tokenType, int numberOfTokens) { 
				return new ArrayList<String>(Arrays.asList("111","222","333","444","555")) ;}
		});
		userService.createUser("kaloyan@penov.com");
		userService.createTokens("kaloyan@penov.com", 5);
		User user = userService.getUser("kaloyan@penov.com");
		// assert
		assertEquals("kaloyan@penov.com", user.getEmail());
		assertEquals(5, user.getTokens().size());
		assertNotNull(user.getTokens().get(0));
		assertEquals("111", user.getTokens().get(0).getId());
		assertEquals("222", user.getTokens().get(1).getId());
	}

	@Test
	public void givenNeededCertainNumberOfTokens_returnListOfTokens() {
		assertEquals(5, userService.generateTokens(5).size());
	}

	@Test(expected = MoreThanOneTokenLeftException.class) // assert
	public void givenUserHasMoreThanOneTokensLeft_returnMoreThanOneTokenLeftException() {
		// arrange
		userService.setUserGateway(new UserGateway() {
			@Override
			public User addUser(User user) { return null;}
			@Override
			public User getUser(String email) {
				User user = new User();
				user.setTokens(new ArrayList<Token>(Arrays.asList(new Token("111"),new Token("222"))));
				return user;
			}
			@Override
			public User getUserByToken(String token) {return null;}
		});
		// act
		userService.createTokens("kaloyan@penov.com",5);
	}

	@Test
	public void givenUserHasOneTokenLeft_returnCreate5MoreTokens() {
		// arrange
		userService.setUserGateway(new UserGateway() {
			@Override
			public User addUser(User user) { return null; }
			@Override
			public User getUser(String email) {
				User user = new User();
				user.setTokens(new ArrayList<Token>(Arrays.asList(new Token("111"))));
				return user;
			}
			@Override
			public User getUserByToken(String token) { return null;	}
		});
		// act
		User user = userService.createTokens("kaloyan@penov.com",5);
		assertEquals(6, user.getTokens().size());
	}
		
	@Test //nobody uses doesTokenBelongToAUser method???
	public void givenTokenOfAUser_returnAUserHasToken() {
		addAUserWithTokenToUserGateway();		
		assertTrue(userService.doesTokenBelongToAUser("Token"));
	}

	@Test //nobody uses doesTokenBelongToAUser method???
	public void givenInvalidTokenOfAUser_returnTokenIsInvalid() {
		addAUserWithTokenToUserGateway();
		assertFalse(userService.doesTokenBelongToAUser("NotAUserToken"));
	}	
	
	@Test
	public void givenClientUsesUnusedTokenThatBelongsToAUser_returnTokenUsed() {
		//arrange
		addAUserWithTokenToUserGateway();				
		//act
		userService.useToken("Token");		
		//assert
		assertEquals("used", userService.getTokenStatus("Token"));
	}

	@Test(expected = TokenUsedException.class)
	public void givenClientUsesUsedTokenThatBelongsToAUser_returnTokenUsedException() {
		//arrange
		addAUserWithTokenToUserGateway();				
		userService.useToken("Token");		
		//act
		userService.useToken("Token");
	}
	
}
