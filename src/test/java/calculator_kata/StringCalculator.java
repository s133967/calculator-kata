package calculator_kata;

public class StringCalculator {
	
	public int add(String str) throws NegativesNotAllowedException {
		if(str.isEmpty())
			return 0;
		else if(isOneCharInString(str))
			return Integer.parseInt(str);		
		else if(isCustomDelimiterPresent(str)) {			
			String delimiter = str.substring(2, 3);
			String[] numbers = str.substring(4, str.length()).split(delimiter);
			return calculateSumOfStrings(numbers);
		}else {
			String[] numbers = str.split("\n|,");
			return calculateSumOfStrings(numbers);
		}
	}

	private boolean isCustomDelimiterPresent(String str) {
		return str.substring(0, 2).equals("//");
	}

	private boolean isOneCharInString(String str) {
		return str.length() == 1;
	}

	private int calculateSumOfStrings(String[] numbers) throws NegativesNotAllowedException {
		int sum = 0;
		String commaSeparatedNegativeNumbers = "";		
		for(String n:numbers) {
			int num = Integer.parseInt(n);			
			sum += num;
			if(num < 0)
				commaSeparatedNegativeNumbers += num + ", ";			
		}
		if(!commaSeparatedNegativeNumbers.isEmpty())
			throw new NegativesNotAllowedException(commaSeparatedNegativeNumbers.substring(0,commaSeparatedNegativeNumbers.length()-2));
		return sum;
	}
	
	public class NegativesNotAllowedException extends Exception{
		private static final long serialVersionUID = -1564260494297324797L;
		public NegativesNotAllowedException(String commaSeparatedNegativeNumbers) {
			super("Negatives not allowed: " + commaSeparatedNegativeNumbers);
		}
	}
	

}