package calculator_kata;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;
//import org.junit.rules.ExpectedException;

import calculator_kata.StringCalculator.NegativesNotAllowedException;

public class AppTest{	

	StringCalculator sc = new StringCalculator();
	
	@Test
	public void givenEmptyString_returnZero() throws NegativesNotAllowedException {
		assertEquals(0, sc.add(""));
	}

	@Test
	public void givenStringWithOneNumber_returnNumber() throws NegativesNotAllowedException{
		assertEquals(1, sc.add("1"));
	}
	
	@Test
	public void givenStringWithTwoCommaSeparatedNumbers_returnTheSum() throws NegativesNotAllowedException{
		assertEquals(3, sc.add("1,2"));
	}
	
	@Test
	public void givenStringWithManyCommaSeparatedNumbers_returnTheSum() throws NegativesNotAllowedException{
		assertEquals(6, sc.add("1,2,3"));
	}
	
	@Test
	public void givenStringWithNewLineSeparatedNumbers_returnTheSum() throws NegativesNotAllowedException{
		assertEquals(6, sc.add("1\n2,3"));
	}
	
	@Test
	public void givenStringOfNumbersSeparatedBySingleCustomDelimiter_returnTheSum() throws NegativesNotAllowedException{
		assertEquals(3, sc.add("//;\n1;2"));
	}
	
	//ExpectedException thrown = ExpectedException.none();
	
	@Test (expected = NegativesNotAllowedException.class)
	public void givenNegativeNumber_returnNegativesNotAllowedExceptionWithMessage() throws NegativesNotAllowedException{		
		//thrown.expect(NegativesNotAllowedException.class);
		//thrown.expectMessage("Negatives not allowed: -1");        
		
		   try{
			   sc.add("-1,2");
		   }catch(NegativesNotAllowedException ex){
		      assertEquals("Negatives not allowed: -1", ex.getMessage());
		      throw ex;
		    }
		    fail("NegativesNotAllowedException did not throw!");
		
	}
	
	
	@Test (expected = NegativesNotAllowedException.class)
	public void givenMultipleNegativeNumbers_returnNegativesNotAllowedExceptionWithMessage() throws NegativesNotAllowedException{		
		//thrown.expect(NegativesNotAllowedException.class);
		//thrown.expectMessage("Negatives not allowed: -1, -3");		
		 try{
			 sc.add("-1,2,-3");
		   }catch(NegativesNotAllowedException ex){
		      assertEquals("Negatives not allowed: -1, -3", ex.getMessage());
		      throw ex;
		    }
		    fail("NegativesNotAllowedException did not throw!");
		
	}
	
	/*
	private int add(String str) throws NegativesNotAllowedException {
		if(str.isEmpty())
			return 0;
		else if(isOneCharInString(str))
			return Integer.parseInt(str);		
		else if(isCustomDelimiterPresent(str)) {			
			String delimiter = str.substring(2, 3);
			String[] numbers = str.substring(4, str.length()).split(delimiter);
			return calculateSumOfStrings(numbers);
		}else {
			String[] numbers = str.split("\n|,");
			return calculateSumOfStrings(numbers);
		}
	}

	private boolean isCustomDelimiterPresent(String str) {
		return str.substring(0, 2).equals("//");
	}

	private boolean isOneCharInString(String str) {
		return str.length() == 1;
	}

	private int calculateSumOfStrings(String[] numbers) throws NegativesNotAllowedException {
		int sum = 0;
		String commaSeparatedNegativeNumbers = "";		
		for(String n:numbers) {
			int num = Integer.parseInt(n);			
			sum += num;
			if(num < 0)
				commaSeparatedNegativeNumbers += num + ", ";			
		}
		if(!commaSeparatedNegativeNumbers.isEmpty())
			throw new NegativesNotAllowedException(commaSeparatedNegativeNumbers.substring(0,commaSeparatedNegativeNumbers.length()-2));
		return sum;
	}

	public class NegativesNotAllowedException extends Exception{
		private static final long serialVersionUID = -1564260494297324797L;
		public NegativesNotAllowedException(String commaSeparatedNegativeNumbers) {
			super("Negatives not allowed: " + commaSeparatedNegativeNumbers);
		}
	}
	*/

}