package barcode_generator;

import static org.junit.Assert.assertEquals;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.security.SecureRandom;
import java.util.UUID;

import org.junit.Test;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Image;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BarcodeEAN;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

public class BarcodeGeneratorTest {
	
	SecureRandom random = new SecureRandom();
	
	@Test
	public void nothing() throws FileNotFoundException, DocumentException {
		/*
		// step 1: creation of a document-object
		Rectangle rect = new Rectangle(243, 153);
		rect.setBackgroundColor(new Color(0xFF, 0xFF, 0xCC));
		Document document = new Document(rect, 10, 10, 10, 10);
		// step 2:
		PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("barcodeDocument.pdf"));//PdfTestBase.getOutputStream("studentcard.pdf"));
		//step 3
		document.open();
		//step 4 add content
		PdfContentByte cb = writer.getDirectContent();				
		BarcodeEAN codeEAN = new BarcodeEAN();
		codeEAN.setCodeType(codeEAN.EAN13);
		codeEAN.setCode("9780201615888");
		Image imageEAN = codeEAN.createImageWithBarcode(cb, null, null);
		
		
		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100);
		table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
		table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.getDefaultCell().setFixedHeight(70);		
		table.addCell("CODE EAN");
		table.addCell(new Phrase(new Chunk(imageEAN, 0, 0)));		
		document.add(table);
		// step 5: we close the document
		document.close();
		
		
		
		String uuid1 = UUID.randomUUID().toString().replace("-","");
		//assertEquals(uuid,"B@5ce65a89");
		
		
		String uuid2 = UUID.randomUUID().toString().replace("-","");
		assertEquals(uuid1,uuid2);
		*/
		//random.nextBytes(bytes);
		//String token2 = bytes.toString();
		//assertEquals(token2,"B@5ce65a89");
	}
	
	
	@Test
	public void givenUnautorisedUserRequestBarcodesByIdForTheFirstTime_returnEmptyRequest() {
		//assertEquals();_
	}
	
	/*
	 * 
	� There is a function to use a token. That means, the software is provided
	with the number/string from one of the issued barcodes and the
	application should accept the token if the token was not used before.
	� If the token was already used before, e.g. presented before, then the
	function should reject the token.
	� If the token is not known to the system (e.g. a fake token), then the
	again the function should reject the token.
	� The customer does not have to register when requesting 1 - 5 tokens
	for the first time. Asking for the tokens will register him automatically.
	� If the user has more than 1 unused token and he requests again a set
	of tokens, his request will be denied
	� Only if he has spent all tokens or has only one unused token left, then
	he can request again 1 to 5 tokens.
	 * */
	
}
