package barcode_generator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class BarcodeServiceTest {
	BarcodeService barcodeService;
	
	@Before
	public void BeforeEachTest() {
		barcodeService = new BarcodeService();
	}
	
	@Test
	public void BarcodeServiceIsNotNull() {		
		assertNotNull(barcodeService);
	}
	
	@Test
	public void givenUnregisteredUserWithValidUserIdRequestsBarcodes_returnListOf5Barcodes() {		
		//arrange
		String userId = "123";
		barcodeService.setUserGateway(new UserGateway(){
			@Override
			public	boolean exists(String userId) {return false;}
		});
		barcodeService.setBarcodeGateway(new BarcodeGateway(){
			@Override
			public	List<String> createBarcodes(String userId){
				return Arrays.asList("Barcode1","Barcode2","Barcode3","Barcode4","Barcode5");
			}
		});
		
		//act
		List<String> barcodes = barcodeService.getBarcodes(userId);
		
		//assert
		assertEquals(5, barcodes.size());
	}
	
	
	
}
