package barcode_generator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

public class BarcodeResponseTest {
	BarcodeResponse barcodeResponse;
	
	@Before
	public void BeforeEachTest() {
		barcodeResponse = new BarcodeResponse();
	}
	
	@Test
	public void BarcodeResponseIsNotNull() {		
		assertNotNull(barcodeResponse);
	}
	
	@Test
	public void givenNewBarcodeResponse_returnEmptyListOfBarcodesAsStrings() {		
		assertEquals(0, barcodeResponse.getBarcodes().size());
	}
	
}
