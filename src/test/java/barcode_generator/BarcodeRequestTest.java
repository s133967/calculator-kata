package barcode_generator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

public class BarcodeRequestTest {
	BarcodeRequest barcodeRequest;
	
	@Before
	public void BeforeEachTest() {
		barcodeRequest = new BarcodeRequest();
	}
	
	@Test
	public void BarcodeRequestIsNotNull() {		
		assertNotNull(barcodeRequest);
	}
	
	@Test
	public void givenNewBarcodeRequest_returnUserIdIsEmptyString() {		
		assertEquals("",barcodeRequest.getUserId());
	}
	
	
	
}
