package services.entities;

public class Token {
	private String status = "unused";
	private String id = "";
	
	public Token() {
		
	}
	public Token(String tokenId) {
		this();
		this.id = tokenId;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public String getStatus() {
		return this.status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getId() {
		return this.id;
	}

}
