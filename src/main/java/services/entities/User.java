package services.entities;

import java.util.ArrayList;
import java.util.List;

public class User {
	private String email = "";
	private List<Token> tokens = new ArrayList<>();
	
	public List<Token> getTokens() {
		return tokens;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public void setTokens(List<Token> tokens) {
		this.tokens = tokens;
		
	}

	public void addTokens(List<Token> tokens) {
		this.tokens.addAll(tokens);
	}

	public boolean hasToken(String tokenId) {
		for(Token t : tokens) {
			if(t.getId().equals(tokenId))
				return true;			
		}
		return false;
	}

}
