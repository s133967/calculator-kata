package services.utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import services.user.TokenGenerator;

public class TokenGeneratorImpl implements TokenGenerator {

	@Override
	public List<String> generate(String tokenType, int numberOfTokens) {
		if(numberOfTokens <= 0 || numberOfTokens > 5)
			throw new InvalidNumberOfTokensException();
		if (tokenType.equals("basic")) {
			List<String> tokens = new ArrayList<>();
			for(int i=0; i < numberOfTokens; i++)
				tokens.add(UUID.randomUUID().toString());
			return tokens;
		} else
			return null;
	}

}
