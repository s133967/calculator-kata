package services.user;

import java.util.List;
import services.entities.User;

public interface UserService {
	public String createUser(String email);
	public User getUser(String email);
	
	public class InvalidUserEmailException extends RuntimeException{
		private static final long serialVersionUID = -1148182704184649203L;		
	}
	public class MoreThanOneTokenLeftException extends RuntimeException{
		private static final long serialVersionUID = 6831874846437440153L;
	}	
	public class TokenUsedException extends RuntimeException{
		private static final long serialVersionUID = -7083679023736421425L;
	}
	
	public void setUserGateway(UserGateway userGateway);
	public void setTokenGenerator(TokenGenerator tokenGenerator);
	public User createTokens(String token, int numberOfTokens);
	public void useToken(String token);
	public String getTokenStatus(String tokenId);
	//public List<String> generateTokens(int numberOfTokens);
}
