package services.user;

import java.util.ArrayList;
import java.util.List;

import services.entities.User;

public class UserGatewayImpl implements UserGateway{

	List<User> users = new ArrayList<>();
	
	@Override
	public User addUser(User user) {
		users.add(user);
		return user;
	}

	@Override
	public User getUser(String email) {		
		for(User u: users) {
			if(u.getEmail().equals(email))
				return u;
		}
		return null;
	}

	@Override
	public User getUserByToken(String token) {		
		for(User u : users) {
			if(u.hasToken(token))
				return u;
		}			
		return null;
	}

}
