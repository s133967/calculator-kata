package services.user;

import java.util.List;

public interface TokenGenerator {
	public class InvalidNumberOfTokensException extends RuntimeException{
		private static final long serialVersionUID = 7377900946839179214L;
	}
	List<String> generate(String tokenType, int numberOfTokens);
}
