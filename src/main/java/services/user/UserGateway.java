package services.user;

import services.entities.User;

public interface UserGateway {
	User addUser(User user);
	User getUser(String email);
	User getUserByToken(String token);
}
