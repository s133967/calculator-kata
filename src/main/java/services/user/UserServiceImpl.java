package services.user;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import services.entities.Token;
import services.entities.User;

public class UserServiceImpl implements UserService{
	private UserGateway userGateway;
	private TokenGenerator tokenGenerator;
	
	@Override
	public String createUser(String email) {
		if(email.isEmpty())
			throw new InvalidUserEmailException();		
		
		User newUser = new User();
		newUser.setEmail(email);
		
		userGateway.addUser(newUser);
		return email;
	}

	@Override
	public User getUser(String email) {		
		return userGateway.getUser(email);
	}

	@Override
	public void setUserGateway(UserGateway userGateway) {
		this.userGateway = userGateway;
	}

	@Override
	public User createTokens(String email, int numberOfTokens) {
		User user = userGateway.getUser(email);
		if(user.getTokens().size() > 1)//check if tokens are used not just the size
			throw new MoreThanOneTokenLeftException();		
		List<String> tokenIds = generateTokens(numberOfTokens);
		for(String ti: tokenIds)
			user.addTokens(new ArrayList<Token>(Arrays.asList(new Token(ti))));			
		return user;
	}
	
	protected List<String> generateTokens(int numberOfTokens) {
		return tokenGenerator.generate("basic", numberOfTokens);
	}

	@Override
	public void setTokenGenerator(TokenGenerator tokenGenerator) {
		this.tokenGenerator = tokenGenerator;
	}

	protected boolean doesTokenBelongToAUser(String token) {
		return (userGateway.getUserByToken(token) == null) ? false : true;
	}

	@Override
	public void useToken(String tokenId) {		
		User user = userGateway.getUserByToken(tokenId);
		List<Token> tokens = user.getTokens();
		for(Token t:tokens) {
			if(t.getId().equals(tokenId)) { 
				if(t.getStatus().equals("used")) 
					throw new TokenUsedException();				
				t.setStatus("used");		
			}	
		}
	}

	public String getTokenStatus(String tokenId) {
		User user = userGateway.getUserByToken(tokenId);
		List<Token> tokens = user.getTokens();
		for(Token t:tokens) {
			if(t.getId().equals(tokenId)) 
				return t.getStatus();			
		}
		return null;
	}

}
