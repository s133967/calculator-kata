package barcode_generator;

import java.util.List;

public interface BarcodeGateway {

	List<String> createBarcodes(String userId);
	

}
