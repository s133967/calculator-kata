package barcode_generator;

import java.util.ArrayList;
import java.util.List;

public class BarcodeResponse {
	private List<String> barcodes = new ArrayList<>();
	public List<String> getBarcodes() {
		return barcodes;
	}

}
