package barcode_generator;

import java.util.UUID;

public class TokenGeneratorImpl implements TokenGenerator{

	@Override
	public String generateToken() {
		return UUID.randomUUID().toString();
	}

}
