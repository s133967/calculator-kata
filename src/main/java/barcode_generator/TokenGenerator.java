package barcode_generator;

public interface TokenGenerator {

	String generateToken();
	
}
