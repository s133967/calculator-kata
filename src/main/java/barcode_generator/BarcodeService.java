package barcode_generator;

import java.util.List;

public class BarcodeService {
	private BarcodeGateway barcodeGateway;
	private UserGateway userGateway;
	

	public List<String> getBarcodes(String userId) {
		if(!userGateway.exists(userId))
			return barcodeGateway.createBarcodes(userId);
		else
			return null;		
	}
	
	protected void setBarcodeGateway(BarcodeGateway barcodeGateway) {
		this.barcodeGateway = barcodeGateway;
	}
	
	protected void setUserGateway(UserGateway userGateway) {
		this.userGateway = userGateway;
	}

}
